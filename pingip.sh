#!/bin/bash

while IFS= read -r line; do
    ping -W 0.3 -c 1 $line && ping -W 0.3 -c 1 $line >> res.txt
    ping -W 0.3 -c 1 $line || ping -W 0.3 -c 1 $line >> err.txt
# works too slow
done < ips.txt


